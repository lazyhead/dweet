# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from dweetapi.models import tweets
from django.db.models import Q
from rest_framework import generics
from rest_framework import permissions
#from django import db
from django.contrib.auth import get_user_model
from dweetapi.serializers import (
    tweetsSerializer,
    commentSerializer,
    followingSerializer,
    likesSerializer,
    retweetSerializer,
    UserSerializer)

class dweetListAPIView(generics.ListAPIView):

    serializer_class=tweetsSerializer

    def get_queryset(self,*args,**kwargs):

        qs=tweets.objects.all().order_by("-created_timestamp")
        #print(self.request.GET)
        query=self.request.GET.get("q",None)
        if query is not None:
            qs=qs.filter(
                Q(content=query)
            )
        return qs

class dweetCreateAPIView(generics.CreateAPIView):
    serializer_class=tweetsSerializer
    permission_classes=[permissions.IsAuthenticated]
    queryset=tweets.objects.all()

    def perform_create(self, serializer):
        serializer.save(parent_id=self.request.user)

class dweetCreateUserAPIView(generics.CreateAPIView):

    model = get_user_model()
    permission_classes = [
        permissions.AllowAny # Or anon users can't register
    ]
    serializer_class = UserSerializer
