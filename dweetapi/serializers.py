from rest_framework import serializers
from dweetapi.models import tweets, comment, following, likes, rewteet
from django.contrib.auth import get_user_model
UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = UserModel.objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.is_staff = True
        user.save()

        return user

    class Meta:
        model = UserModel
        fields=('username','first_name','last_name','password')

class tweetsSerializer(serializers.ModelSerializer):
    class Meta:
        model=tweets
        fields=('tweet_id','created_timestamp','updated_timestamp','content')



class commentSerializer(serializers.ModelSerializer):
    class Meta:
        model=comment
        fields=('comment_id','user_id','tweet_id','created_timestamp','updated_timestamp','content')

class followingSerializer(serializers.ModelSerializer):
    class Meta:
        model=following
        fields=('following_id','user_id_follower','user_id_followed','timestamp')

class likesSerializer(serializers.ModelSerializer):
    class Meta:
        model=likes
        fields=('like_id','tweet_id','user_id','timestamp')
class retweetSerializer(serializers.ModelSerializer):
    class Meta:
        model=rewteet
        fields=('retweet_id','user_id','tweet_id','timestamp')

