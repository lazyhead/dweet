# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

# Create your models here.

class tweets(models.Model):
    tweet_id            =models.AutoField(primary_key=True)
    created_timestamp   =models.DateTimeField(auto_now_add=True)
    updated_timestamp   =models.DateTimeField(auto_now=True)
    content             =models.CharField(null=False,max_length=140)
    parent_id           =models.ForeignKey(settings.AUTH_USER_MODEL)

    def __unicode__(self):
        return self.tweet_id

class comment(models.Model):
    comment_id          =models.AutoField(primary_key=True)
    user_id             =models.ForeignKey(User)
    tweet_id            =models.ForeignKey(tweets)
    created_timestamp   =models.DateTimeField(auto_now_add=True)
    updated_timestamp   =models.DateTimeField(auto_now=True)
    content = models.CharField(null=False, max_length=140)

    def __unicode__(self):
        return self.comment_id

class following(models.Model):
    following_id        =models.AutoField(primary_key=True)
    user_id_follower    =models.ForeignKey(User,related_name='follower')
    user_id_followed    =models.ForeignKey(User,related_name='followed')
    timestamp           =models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.following_id

class likes(models.Model):
    like_id             =models.AutoField(primary_key=True)
    tweet_id            =models.ForeignKey(tweets)
    user_id             =models.ForeignKey(User)
    timestamp           =models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.like_id

class rewteet(models.Model):
    retweet_id          =models.AutoField(primary_key=True)
    user_id             =models.ForeignKey(User)
    tweet_id            =models.ForeignKey(tweets)
    timestamp           =models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.retweet_id
