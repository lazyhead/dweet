from django.conf.urls import url,include
from rest_framework.urlpatterns import format_suffix_patterns
from dweetapi.views import dweetListAPIView , dweetCreateAPIView, dweetCreateUserAPIView


urlpatterns = [
    # url(r'^$', RedirectView.as_view(url="/")),
    url(r'^$', dweetListAPIView.as_view(), name='list'), # /api/tweet/
    url(r'^create/$', dweetCreateAPIView.as_view(), name='create'), # /tweet/create/
    url(r'^cUser/$', dweetCreateUserAPIView.as_view(), name='create'),
    #url(r'^(?P<pk>\d+)/$', TweetDTweetCreateAPIViewetailAPIView.as_view(), name='detail'),
    #url(r'^(?P<pk>\d+)/like/$', LikeToggleAPIView.as_view(), name='like-toggle'),
    #url(r'^(?P<pk>\d+)/retweet/$', RetweetAPIView.as_view(), name='retweet'),
]

urlpatterns=format_suffix_patterns(urlpatterns)